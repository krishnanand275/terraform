Role Name: terraform
An ansible role to deploy terraform project. It has these funtions
  1. Configure terraform
  2. Setup project
  3. Terraform action


 1.  - Configure terraform will install terraform if it is not installed. 
     - User can specify version of terraform using terraform_version variable .
     - User can update current version of terraform. by specifying update_terrafrom value to true.

 2. - User can specify existing path of the project by specifing project_path variable.
    - User can clone a git repo by using use_git_repo=true and provide "git_repo_url".
    - User can input new path also.

 3. - User can either choose planned or present state for terraform project.
    - User can specify plan file for plan and deploy action.

Requirements
Ansible 2.5+ version is required. 

Role Variables

Available variables are listed below, along with default values (see defaults/main.yml):


project_path: "/terraform/sampleterrform/"

Path for the terraform project.

project_name: "project1"
 
Project name to include varaible file .

plan_file: "plan1.tfplan"

plan file name for terrafrom projects.

value1: "value"

Ansible vars for terrafrom .

backend_value1: "backvalue"
backend_value2: "backvalue"

Terraform backend configration variables.

state: "planned"
Action for terrafrom . values can be "planned", "present" or "absent"

terraform_version: "0.11.11"

Version for terrafrom.

use_git_repo: "no"

To enable use of git repository,

git_repo_url: "https://github.com/krishnanand275/sampleterraform.git"

Repository URL for exixting terraform projects.

update_terraform: false

Update exiting terrafrom can be done making this variable values "true"


Dependencies


Example Playbook
Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

---
- hosts: localhost
  connection: local
  roles:
    - terraform

License
BSD

Author Information
Krishna nand chpudhary
